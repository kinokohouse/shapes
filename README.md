# Shapes #
The simplest of all simple screen savers, save perhaps for a black screen.
Detects when your screen goes to sleep, at which point it will draw a black screen
and go into 'quiet' mode in which it won't be drawing ehm, shapes anymore.
No other options available. Binary supplied in the downloads section. MIT License.

Building
--------
Project is made in Xcode 10.3. Currently builds for macOS 10.12 and up.

Version history
---------------
### 1.1 (build 2, current): ###
* Better distribution of objects.

### 1.0 (build 1): ###
* Initial release.
