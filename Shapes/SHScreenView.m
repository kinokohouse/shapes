//
//  SHScreenView.m
//  Shapes
//
//  Created by Petros Loukareas on 06/09/2019.
//  Copyright © 2019 Petros Loukareas. All rights reserved.
//

#import "SHScreenView.h"

@implementation SHScreenView

- (instancetype)initWithFrame:(NSRect)frame isPreview:(BOOL)isPreview {
    self = [super initWithFrame:frame isPreview:isPreview];
    if (self) {
        [self setAnimationTimeInterval:0.5];
        self.objectList = [[NSMutableArray alloc] init];
        self.backgroundColorList = @[[NSColor colorWithRed:0.2 green:0 blue:0 alpha:1.0], [NSColor colorWithRed:0 green:0.2 blue:0 alpha:1.0], [NSColor colorWithRed:0 green:0 blue:0.2 alpha:1.0], [NSColor colorWithRed:0.2 green:0.2 blue:0 alpha:0.7], [NSColor colorWithRed:0.2 green:0 blue:0.2 alpha:0.7], [NSColor colorWithRed:0 green:0.2 blue:0.2 alpha:0.7]];
        self.stepCounter = 0;
        self.flipCounter = 0;
        self.colorPointer = 0;
        [[[NSWorkspace sharedWorkspace] notificationCenter]addObserver:self selector:@selector(screenDidSleep) name:NSWorkspaceScreensDidSleepNotification object:nil];
        [[[NSWorkspace sharedWorkspace] notificationCenter]addObserver:self selector:@selector(screenDidWake) name:NSWorkspaceScreensDidWakeNotification object:nil];
    }
    return self;
}

- (void)startAnimation {
    [super startAnimation];
}

- (void)stopAnimation {
    [super stopAnimation];
}

- (void)drawRect:(NSRect)rect {
    if (_sleeping) {
        if ([_objectList count] > 0) {
            [super drawRect:rect];
            [self setAnimationTimeInterval:30];
            _objectList = [@[] mutableCopy];
            return;
        } else {
            return;
        }
    }
    [super drawRect:rect];
    [[_backgroundColorList objectAtIndex:_colorPointer] setFill];
    NSRectFill([self bounds]);
    BOOL oval = NO;
    CGFloat shapeDecisor = SSRandomIntBetween(0, 1);
    if (shapeDecisor > 0.49) {
        oval = TRUE;
    }
    NSColor *fillColor = [NSColor colorWithRed:SSRandomFloatBetween(0, 1) green:SSRandomFloatBetween(0, 1) blue:SSRandomFloatBetween(0, 1) alpha:SSRandomFloatBetween(0.5, 1)];
    NSColor *strokeColor = [NSColor colorWithRed:SSRandomFloatBetween(0, 1) green:SSRandomFloatBetween(0, 1) blue:SSRandomFloatBetween(0, 1) alpha:SSRandomFloatBetween(0.5, 1)];
    NSUInteger rectOrigX = SSRandomFloatBetween(0, ([self bounds].size.width - 80));
    NSUInteger rectOrigY = SSRandomFloatBetween(0, ([self bounds].size.height - 80));
    NSUInteger rectWidth = SSRandomFloatBetween(80, ([self bounds].size.width - rectOrigX));
    NSUInteger rectHeight = SSRandomFloatBetween(80, ([self bounds].size.height - rectOrigY));
    if (_flipCounter == 1 || _flipCounter == 3) {
        rectOrigX = [self bounds].size.width - rectOrigX - rectWidth;
    }
    if (_flipCounter > 1) {
        rectOrigY = [self bounds].size.height - rectOrigY - rectHeight;
    }
    NSRect aRect = NSMakeRect(rectOrigX, rectOrigY, rectWidth, rectHeight);
    SHObject *theObject = [[SHObject alloc] initWithRect:aRect fillColor:fillColor strokeColor:strokeColor isOval:oval];
    [_objectList addObject:theObject];
    if ([_objectList count] > 20) [_objectList removeObjectAtIndex:0];
    for (int i = 0; i < [_objectList count]; i++) {
        SHObject *currentObject = _objectList[i];
        NSColor *aColor = [currentObject fillColor];
        NSColor *bColor = [currentObject strokeColor];
        NSRect aRect = [currentObject objRect];
        [aColor setFill];
        [bColor setStroke];
        NSBezierPath *path = [NSBezierPath bezierPath];
        [path setLineWidth:0.0f];
        if ([currentObject isOval]) {
            [path appendBezierPathWithOvalInRect:aRect];
        } else {
            [path appendBezierPathWithRoundedRect:aRect xRadius:10 yRadius:10];
        }
        [path fill];
        [path stroke];
    }
    _stepCounter++;
    if (_stepCounter > 19) {
        _stepCounter = 0;
        _colorPointer++;
        if (_colorPointer > 5) {
            _colorPointer = 0;
        }
    }
    _flipCounter ++;
    if (_flipCounter > 3) _flipCounter = 0;
}

- (void)animateOneFrame {
    [self setNeedsDisplay:YES];
}

- (BOOL)hasConfigureSheet {
    return NO;
}

- (NSWindow*)configureSheet {
    return nil;
}
    
- (void)screenDidSleep {
    _sleeping = YES;
}
    
- (void)screenDidWake {
    [self setAnimationTimeInterval:0.5];
    _sleeping = NO;
}

@end
