//
//  SHScreenView.h
//  Shapes
//
//  Created by Petros Loukareas on 06/09/2019.
//  Copyright © 2019 Petros Loukareas. All rights reserved.
//

#import <ScreenSaver/ScreenSaver.h>
#import "SHObject.h"

@interface SHScreenView : ScreenSaverView

@property (strong) NSMutableArray <SHObject *> *objectList;
@property (strong) NSArray <NSColor *> *backgroundColorList;
@property (assign) NSUInteger stepCounter;
@property (assign) NSUInteger flipCounter;
@property (assign) NSUInteger colorPointer;
@property (assign) BOOL sleeping;

@end
