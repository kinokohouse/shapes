//
//  SHObject.h
//  Shapes
//
//  Created by Petros Loukareas on 07/09/2019.
//  Copyright © 2019 Petros Loukareas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface SHObject : NSObject

@property (assign) NSRect objRect;
@property (strong) NSColor *fillColor;
@property (strong) NSColor *strokeColor;
@property (assign) BOOL isOval;

- (instancetype)init;
- (instancetype)initWithRect:(NSRect)aRect fillColor:(NSColor *)aColor strokeColor:(NSColor *)bColor isOval:(BOOL)ovalFlag;

@end

NS_ASSUME_NONNULL_END
