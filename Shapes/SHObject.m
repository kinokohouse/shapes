//
//  SHObject.m
//  Shapes
//
//  Created by Petros Loukareas on 07/09/2019.
//  Copyright © 2019 Petros Loukareas. All rights reserved.
//

#import "SHObject.h"

@implementation SHObject

- (instancetype)init {
    if (self = [super init]) {
        self.objRect = NSZeroRect;
        self.fillColor = [NSColor blackColor];
    }
    return self;
}

- (instancetype)initWithRect:(NSRect)aRect fillColor:(NSColor *)aColor strokeColor:(NSColor *)bColor isOval:(BOOL)ovalFlag{
    if (self = [super init]) {
        self.objRect = aRect;
        self.fillColor = aColor;
        self.strokeColor = bColor;
        self.isOval = ovalFlag;
    }
    return self;
}

@end
